/**
 * @module queue
 *
 **/
;
(function (context) {
	console.log(context);
	'use strict';
	var isAmd = typeof define === 'function' && define.amd,
		isNode = ((typeof module === 'object') && (typeof module.exports === 'object')),
		isClient = !isAmd && !isNode;
	if (isNode) {
		var EventEmitter = require("events").EventEmitter,
			util = require("util");
	} else {
		var EventEmitter =  context.EventEmitter;
	}
	function Queue(queueName) {
		if (!(this instanceof Queue)) {
			return new Queue(queueName);
		}
		if (!isNode) {
			this.prototype = Object.create(context.EventEmitter.__proto__);
		}	;
		EventEmitter.call(this);
		var _status = {queueName: queueName, lastEvent: "initialized", queueLength: 0, maxWorkers: 0, workers: 0};
		var _queueName = queueName;
		var _eventQueue = [];
		var _maxWorkers = 0;
		var _workers = 0;
		var self = this;
		var _lastEvent = "";
		var _checkQueue = function () {
			if (self.listeners("process").length > 0) {
				if ((_eventQueue.length > 0) && (_workers < _maxWorkers)) {
					_workers++;
					self.emit('process', _eventQueue.shift());
					_lastEvent = "process";
					self.emit("statusChanged");
					_checkQueue();
				}
			} else {
				_lastEvent = "no listeners";
				self.emit("statusChanged");
			}
		};
		this._getStatus = function () {
			return _status;
		}
		if(isNode) {
			this.setMaxListeners(1);
		}
		if(isNode){
			this.addListener("statusChanged", function () {
				_status = {
					queueName  : queueName,
					lastEvent  : _lastEvent,
					queueLength: _eventQueue.length,
					maxWorkers : _maxWorkers,
					workers    : _workers
				};
				this.emit("log", _status);
			})
			this.addListener('addWorker', function (maxWorkers) {
				_maxWorkers += maxWorkers;
				_lastEvent = "added worker";
				self.emit("statusChanged");
				process.nextTick(function () {
					_checkQueue();
				})
			});
			this.addListener("workerDone", function () {
				_workers--;
				_lastEvent = "finished";
				self.emit("statusChanged");
				_checkQueue();
			})
			this.addListener("pushToQueue", function (item) {
				_eventQueue.push(item);
				_lastEvent = "push to queue";
				self.emit("statusChanged");
				_checkQueue();
			})
		}else {

			this.prototype.addListener("statusChanged", function () {
				_status = {
					queueName  : queueName,
					lastEvent  : _lastEvent,
					queueLength: _eventQueue.length,
					maxWorkers : _maxWorkers,
					workers    : _workers
				};
				this.emit("log", _status);
			})
			this.prototype.addListener('addWorker', function (maxWorkers) {
				_maxWorkers += maxWorkers;
				_lastEvent = "added worker";
				self.emit("statusChanged");
				process.nextTick(function () {
					_checkQueue();
				})
			});
			this.prototype.addListener("workerDone", function () {
				_workers--;
				_lastEvent = "finished";
				self.emit("statusChanged");
				_checkQueue();
			})
			this.prototype.addListener("pushToQueue", function (item) {
				_eventQueue.push(item);
				_lastEvent = "push to queue";
				self.emit("statusChanged");
				_checkQueue();
			})
		}
	}

	if (isNode) {
		util.inherits(Queue, EventEmitter);
	} else {
		Queue.prototype = Object.create(context.EventEmitter.prototype);
	}
	Queue.prototype.getStatus = function () {
		return this._getStatus();
	}
	if (isAmd) {
		define(function () {
			return Queue;
		})
	} else if (isNode) {
		module.exports = Queue;
	} else {
		context.Queue = Queue;
	}

})(this);
//test
