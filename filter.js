/**
 * module filter
 *
 */
'use strict';
module.exports=(obj,predicate)=>{
  let a=Object.keys(obj).reduce((p,c)=>{
    if (predicate(c)){
      p.push(obj[c]);
    }
    return p;
  },[]);
  return a;
}