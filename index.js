/**
 * @module index
 * Main code to interact with tedious
 * After requiring the module you must call init
 * All functions are asynchronous and return promises
 * including the init function
 *
 **/
var out = {
	serialRunner: require("./serialPromiseRunner"),
	combineArray:require("./combineArray"),
	Queue:require("./queueClient"),
	trim:require("./trim"),
  extendNE:require("./extendNE"),
  encrypt:require("./encrypt"),
  filter:require("./filter")
}
module.exports = function () {
	return out;
}()
