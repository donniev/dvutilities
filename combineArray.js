/**
 * Combines two arrays
 * @param a destination array
 * @param b array to be added
 * @param pos insertion point use a.length to append default 0
 * @returns {*} returns a
 */
module.exports = (a, b, pos)=> {
  pos = pos || 0;
  if (!(toString.call(a) === '[object Array]')) {
    a = b;
  } else {
    if (!(toString.call(b) === '[object Array]')) {
      b = [b];
    }
    b.unshift(0);
    b.unshift(0);
    b[0] = pos;
    a.splice.apply(a, b);
  }
  return a;
}