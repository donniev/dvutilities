/**
 * module trim
 *
 */
module.exports = function (inString) {
  if (!((typeof inString === 'string') || (toString.apply(inString) === '[object String]'))) {
    return inString;
  }
  if (String.prototype.trim) {
    return inString.trim();
  } else {
    return inString.toString().replace(/^ +/, "").replace(/ +$/, "");
  }
};
