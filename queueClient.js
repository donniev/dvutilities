/**
 * queueClient
 * Creates a throttled queue
 * It may be used within node or in browser
 * If used in the browser you must include the EventEmitter.js from the wolfy97-eventemitter
 * module in your page before including this file.
 * On the server that module is not necessary because we use Nodes native Event emitter
 *
 */
;
(function () {
	'use strict';
	/**
	 * Determine the calling context
	 */
	var isAmd = typeof define === 'function' && define.amd,
		isNode = ((typeof module === 'object') && (typeof module.exports === 'object'))
	if (isNode) {
		var EventEmitter = require("events").EventEmitter,
			util = require("util");
	} else {
		var EventEmitter = this.EventEmitter;
	}
	/**
	 *
	 * @param queueName
	 * @constructor
	 */
	function Queue(queueName) {
		//call the superclass constructor
		EventEmitter.call(this);
		// leading _ implies private
		var _status = {queueName: queueName, lastEvent: "initialized", queueLength: 0, maxItems: 0, workers: 0};
		var _queueName = queueName;
		var _eventQueue = [];
		var _maxItems = 0;
		var _workers = 0;
		var self = this;
		var _lastEvent = "";
		/**
		 *  checks the queue and if there are items on it and we have listeners and we
		 *  have not exceeding maximum number of simulatenous processes we pop an item
		 *  off the queue and emit a "process" event to the listener and then check the queue again.
		 *  You may use the events dequeue or popqueue as alias's for process;
		 * @private
		 */
		var _checkQueue = function () {
			if (self.listeners("process").length > 0) {
				if ((_eventQueue.length > 0) && (_workers < _maxItems)) {
					_workers++;
					var item= _eventQueue.shift();
					self.emit('process',item);
					self.emit('dequeue',item);
					self.emit('pop',item);
					_lastEvent = "process";
					self.emit("statusChanged");
					_checkQueue();
				}
			} else {
				_lastEvent = "no listeners";
				self.emit("statusChanged");
			}
		};
		/**
		 * returns the current status of queue
		 * @returns {{queueName: *, lastEvent: string, queueLength: number, maxItems: number, workers: number}}
		 * @private
		 */
		this._getStatus = function () {
			return _status;
		}
		/**
		 * it makes no sense to have multiple listeners processing the same event
		 * so we protect ourselves. There is no client side equivalent of setMaxListeners
		 *
		 */
		if (isNode) {
			this.setMaxListeners(1);
		}
		/**
		 *  Updates the status of the queue. Users of the queue should not need to emit this event.
		 */
		this.on("statusChanged", function () {
			_status = {
				queueName  : queueName,
				lastEvent  : _lastEvent,
				queueLength: _eventQueue.length,
				maxItems : _maxItems,
				workers    : _workers
			};
			//This is a no op unless you have added a listener to the log event
			this.emit("log", _status);
		})
		/**
		 * Listens for the addition of a worker which can process a queue item
		 * maxItems is the maximumn number of items the worker can simultaneously process
		 */
		this.on('addWorker', function (maxItems) {
			_maxItems += maxItems;
			_lastEvent = "added worker";
			self.emit("statusChanged");
			if (typeof process != 'undefined') {
				process.nextTick(function () {
					_checkQueue();
				})
			} else {
				setTimeout(function () {

				}, 0);
			}
		});
		/**
		 * The processor must notify the queue when it is finished or the queue will stall
		 * This is true even if your processing should fail.
		 */
		this.on("workerDone", function () {
			_workers--;
			_lastEvent = "finished";
			self.emit("statusChanged");
			_checkQueue();
		});
		/**
		 * Add items to the queue by emitting this event.
		 * item is whatever you want. Item will be sent to
		 * the listener which processes queue items.
		 */
		this.on("pushToQueue", function (item) {
			_eventQueue.push(item);
			_lastEvent = "push to queue";
			self.emit("statusChanged");
			_checkQueue();
		});
		/**
		 * Alias for pushToQueue
		 */
		this.on("push", function (item) {
			_eventQueue.push(item);
			_lastEvent = "push to queue";
			self.emit("statusChanged");
			_checkQueue();
		});
		/**
		 * Alias for pushToQueue
		 */
		this.on("enqueue", function (item) {
			_eventQueue.push(item);
			_lastEvent = "push to queue";
			self.emit("statusChanged");
			_checkQueue();
		});
	}

	//set up inheritance
	if (isNode) {
		util.inherits(Queue, EventEmitter);
	} else {
		Queue.prototype = Object.create(this.EventEmitter.prototype);
	}
	/**
	 * Public method for retrieving queue status
	 * @returns {{queueName: *, lastEvent: string, queueLength: number, maxItems: number, workers: number}}
	 */
	Queue.prototype.getStatus = function () {
		return this._getStatus();
	}
	/**
	 * The client emitter does not have the listeners function but has getListeners
	 * which is equivalent (at least for our purposes)
	 */
	if (!Queue.prototype.listeners) {
		Queue.prototype.listeners = Queue.prototype.getListeners;
	}
	//handling context
	var exports = this;
	if (typeof define === 'function' && define.amd) {
		define(function () {
			return Queue;
		});
	}
	else if (typeof module === 'object' && module.exports) {
		module.exports = Queue;
	}
	else {
		exports.Queue = Queue;
	}
}.call(this));