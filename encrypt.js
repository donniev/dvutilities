'use strict';
module.exports =  (st)=> {
  let mapping = "abcdefghijklmnopqrstuvwxyz",
    a = mapping.split(""),
    b = mapping.split(""),
    ct = 0,
    total = 100;
  while (ct < total) {
    b.sort( (i, j)=> ((Math.random() < 0.5) ? -1 : 1));
    var good = true;
    a.forEach( (item, index)=> good = good && (item !== b[index]));
    if (good) {
      break;
    }
    ct++;
  }
  if (ct >= total) {
    console.log("cannot decrypt")
    return null;
  } else {
    let transform = a.reduce(function (prev, curr, index) {
      prev[curr] = b[index];
      return prev;
    }, {});
    let out = st.toLowerCase().split("").map( (item)=> transform[item] ? transform[item] : item);
    return out.join("");
  }
}