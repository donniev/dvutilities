/**
 * Created by donvawter on 4/5/15.
 */
var util=require("../");
var should = require("chai").should();
describe("queue one process at a time one one queue",function() {
	var q = new util.Queue("testque");
	q.on("log",function(status){
		console.log(status);
	});
	it("initialize", function () {
		var status = q.getStatus();
		status.lastEvent.should.equal("initialized")
	});
	it("push to queue", function () {
		q.emit("push", {name: "first", waitTime: 3000});
		var status = q.getStatus();
		status.should.deep.equal({
			queueName  : 'testque',
			lastEvent  : 'no listeners',
			queueLength: 1,
			maxItems : 0,
			workers    : 0
		});
	});
	it("add worker", function (done) {
		q.emit("addWorker", 1);
		q.on("process", function (item) {
			setTimeout(function () {
				q.emit("workerDone");
			}, item.waitTime);
		})
		var status = q.getStatus();
		status.should.deep.equal({
			queueName  : 'testque',
			lastEvent  : 'added worker',
			queueLength: 1,
			maxItems : 1,
			workers    : 0
		});
		process.nextTick(function(){
			status = q.getStatus();
			status.should.deep.equal({
				queueName  : 'testque',
				lastEvent  : 'process',
				queueLength: 0,
				maxItems : 1,
				workers    : 1
			});
			done();
		})
	});
	it("pop", function (done) {
		setTimeout(function () {
			var status = q.getStatus();
			status.should.deep.equal({
				queueName  : 'testque',
				lastEvent  : 'process',
				queueLength: 0,
				maxItems : 1,
				workers    : 1
			});
			done();
		}, 1000);

	});
	it("finished", function (done) {
		setTimeout(function () {
			var status = q.getStatus();
			status.should.deep.equal({
				queueName  : 'testque',
				lastEvent  : 'finished',
				queueLength: 0,
				maxItems : 1,
				workers    : 0
			});
			done();
		}, 3000);

	});
});
