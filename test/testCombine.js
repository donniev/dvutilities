/**
 * @module testCombine
 *
 **/
var should = require("chai").should();
var utils = require("../index");
describe("combine", function () {
	it("combine two arrays", function () {
		var a = [1, 2, 3];
		var b = [4,  5, 6];
		utils.combineArray(a, b);
		return a.should.deep.equal([4, 5, 6, 1, 2, 3]);
	});
	it("combine two arrays b at end", function () {
		var a = [1, 2, 3];
		var b = [4,  5, 6];
		utils.combineArray(a, b,3);
		return a.should.deep.equal([ 1, 2, 3,4,5,6]);
	});
	it("array with empty array", function () {
		var a = [1, 2, 3];
		var b = [];
		utils.combineArray(a, b);
		return a.should.deep.equal([ 1, 2, 3]);
	});
	it("array with null", function () {
		var a = [1, 2, 3];
		var b = null;
		utils.combineArray(a, b);
		return a.should.deep.equal([ 1, 2, 3]);
	});
	it("null with array", function () {
		var a = [1, 2, 3];
		var b = null;
		utils.combineArray(b, a);
		return a.should.deep.equal([ 1, 2, 3]);
	});
	it("null with array", function () {
		var a = null;
		var b = null;
		utils.combineArray(b, a);
		return a===null;
	});
});
