var Promise=Promise || require("bluebird");
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var utils = require("../index");
var SRunner = utils.serialRunner;
var should = require("chai").should();
var first = function (arg1, arg2, results) {
	return new Promise(function (resolve, reject) {
		process.nextTick(function () {
			resolve(arg1 + arg2)
		})
	})
}
var second = function (arg1, arg2, arg3, results) {
	return new Promise(function (resolve, reject) {
		process.nextTick(function () {
			//console.log("results when I was called", results);
			resolve(arg1 + arg2 + arg3);
		})
	})
}
var third = function (arg1, arg2, arg3, results) {
	return new Promise(function (resolve, reject) {
		process.nextTick(function () {
			//console.log("results when I was called", results);
			reject(new Error("bad input"));
		})
	})
}
describe("single task", function (done) {
	it("(no error)", function () {
		var f = new SRunner();
		f.addTask({name: "firstOne", funct: first, args: [1, 2]});
		return f.runTasks().should.eventually.deep.equal({
			firstOne : {success: true, results: 3}
		});
	})
	it("(error)", function () {
		var f = new SRunner();
		f.addTask({name: "thirdOne", funct: third, args: [1, 2,3]});
		return f.runTasks().should.eventually.deep.equal({
			thirdOne : {success: false, error: new Error("bad input")}
		});
	})
})
describe("multiple tasks", function (done) {
	it("no errors", function () {
		var f = new SRunner();
		f.addTask({name: "firstOne", funct: first, args: [1, 2]});
		f.addTask({name: "secondOne", funct: second, args: [3, 4, 5]});
		return f.runTasks().should.eventually.deep.equal({
			firstOne : {success: true, results: 3},
			secondOne: {success: true, results: 12}
		});
	})
	it("last test errors", function () {
		var f = new SRunner();
		f.addTask({name: "firstOne", funct: first, args: [1, 2]});
		f.addTask({name: "secondOne", funct: second, args: [3, 4, 5]});
		f.addTask({name: "thirdOne", funct: third, args: [3, 4, 5]});
		return f.runTasks().should.eventually.deep.equal({
			firstOne : {success: true, results: 3},
			secondOne: {success: true, results: 12},
			thirdOne : {success: false, error: new Error("bad input")}
		});
	})
	it("first test errors", function () {
		var f = new SRunner();
		f.addTask({name: "thirdOne", funct: third, args: [3, 4, 5]});
		f.addTask({name: "firstOne", funct: first, args: [1, 2]});
		f.addTask({name: "secondOne", funct: second, args: [3, 4, 5]});
		return f.runTasks().should.eventually.deep.equal({
			firstOne   : {success: true, results: 3},
			"secondOne": {
				"results": 12,
				"success": true
			},
			thirdOne   : {success: false, error: new Error("bad input")}
		});
	});
})
describe("multiple tests stop on error", function (done) {
	it("first test(no error)", function () {
		var f = new SRunner();
		f.addTask({name: "firstOne", funct: first, args: [1, 2]});
		f.addTask({name: "secondOne", funct: second, args: [3, 4, 5]});
		return f.runTasks().should.eventually.deep.equal({
			firstOne : {success: true, results: 3},
			secondOne: {success: true, results: 12}
		});
	})
	it("middle test errors", function () {
		var f = new SRunner();
		f.addTask({name: "firstOne", funct: first, args: [1, 2]});
		f.addTask({name: "thirdOne", funct: third, args: [3, 4, 5]});
		f.addTask({name: "secondOne", funct: second, args: [3, 4, 5]});
		return f.runTasksStopOnError().should.eventually.deep.equal({
			firstOne : {success: true, results: 3},
			secondOne: {success: true, results: null},
			thirdOne : {success: false, error: new Error("bad input")}
		});
	});
	it("initial test errors", function () {
		var f = new SRunner();
		f.addTask({name: "thirdOne", funct: third, args: [3, 4, 5]});
		f.addTask({name: "firstOne", funct: first, args: [1, 2]});
		f.addTask({name: "secondOne", funct: second, args: [3, 4, 5]});
		return f.runTasksStopOnError().should.eventually.deep.equal({
			firstOne: {success: true, results: null},
			thirdOne: {success: false, error: new Error("bad input")}
		});
	});
})
