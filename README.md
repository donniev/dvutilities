##DVUtilities
A simple module to encapsulate utilities

Current helpers

1. serialPromiseRunner - runs a set of promises serially
1. combineArray - combines second array to first. (inserts at begining by default)
1. queue - queing utility

###Queue explanation
Allows you set up a throttled queue. The code may be run on a node server or in
a browser. It inherits from Nodes EventEmitter on the server and from Wolfy87-eventemitter in the browser.
###Events
1. pushToQueue(item) -- add an item to the queue (alias's push, enqueue)
2. addWorker(maxItems) -- maxItems is the maximum items the worker can process simultaneously.
3. process(item) -- emitted by queue manager when not throttled (alias's pop, dequeue)
3. workerDone -- the processor of the item must notify the queue when finished by emitting this event.
4. statusChanged -- used internally
5. log -- emitted when the status of the queue changes. This is a no op unless you add a listener.


###Usage combineArray

```
var utils=require("dvutilities");

var a=[1,2,3];
var b=[4,5,6];
utils.combineArray(a,b,0) //after a=[4,5,6,1,2,3]
var a=[1,2,3];
var b=[4,5,6];
utils.combineArray(a,b,3) //after a=[1,2,3,4,5,6]

```
###Usage serialPromiseRunner
```
var SRunner=utils.serialRunner;
var tasks=[];
var myFunct=function(a,b,c){
	return new Promise(function(resolve,reject){
		if((a+b+c)<10){
			process.nextTick(function(){
				resolve("ok");
			});
		}else{
			process.nextTick(function(){
				reject(new Error("Too big"));
			});
		}
	})
}
tasks.push({name:"myTask",funct:myFunct,args:[1,2,3]},{name:"mySecondTask",funct:myFunct,args:[1,2,13]});
var sr=new SRunner(tasks);
sr.runTasks()
	.then(function(results){
		console.log(results); //{myTask:{success:true,results:"ok",mySecondTask:{success:false,error:Error}} error message Too Big
	})
	.catch(function(er){
		console.log(er);
	});
```
### Queue Usage

####Server Side
```
 var Queue=require("dvutilities").Queue;
 var q = new Queue("myQueName");
 q.on("process",function(item){
   setTimeout(function(){
      console.log(item);
      q.emit("workerDone");
   },item.waitTime);
  });
  q.on("log",function(status){
     console.log(status);
   });
  q.emit("addWorker",2);
  q.emit("pushToQueue",{name:"first",waitTime:3000}); 
  q.emit("pushToQueue",{name:"second",waitTime:1000});
  q.emit("pushToQueue",{name:"third",waitTime:6000});
```
  This would have the following output:

```
  { queueName: 'myQueName',
  lastEvent: 'added worker',
  queueLength: 0,
  maxItems: 2,
  workers: 0 }
{ queueName: 'myQueName',
  lastEvent: 'push to queue',
  queueLength: 1,
  maxItems: 2,
  workers: 0 }
{ queueName: 'myQueName',
  lastEvent: 'process',
  queueLength: 0,
  maxItems: 2,
  workers: 1 }
{ queueName: 'myQueName',
  lastEvent: 'push to queue',
  queueLength: 1,
  maxItems: 2,
  workers: 1 }
{ queueName: 'myQueName',
  lastEvent: 'process',
  queueLength: 0,
  maxItems: 2,
  workers: 2 }
{ queueName: 'myQueName',
  lastEvent: 'push to queue',
  queueLength: 1,
  maxItems: 2,
  workers: 2 }
{ name: 'second', waitTime: 1000 }
{ queueName: 'myQueName',
  lastEvent: 'finished',
  queueLength: 1,
  maxItems: 2,
  workers: 1 }
{ queueName: 'myQueName',
  lastEvent: 'process',
  queueLength: 0,
  maxItems: 2,
  workers: 2 }
{ name: 'first', waitTime: 3000 }
{ queueName: 'myQueName',
  lastEvent: 'finished',
  queueLength: 0,
  maxItems: 2,
  workers: 1 }
{ name: 'third', waitTime: 6000 }
{ queueName: 'myQueName',
  lastEvent: 'finished',
  queueLength: 0,
  maxItems: 2,
  workers: 0 }

```

####Client Side
```
<html>
<head>
  <script src="./node_modules/wolfy87-eventemitter/EventEmitter.js" type="application/javascript"></script>
  <script src="./queueClient.js" type="application/javascript"></script>
  <script language="JavaScript">
    console.log(EventEmitter);
    var q = new Queue("myQueName");
    console.log(q instanceof EventEmitter);
    q.on("log", function (status) {
      console.log(status);
    });
    q.emit("addWorker", 2);
    q.on("process", function (item) {
      console.log(item);
      setTimeout(function () {
        q.emit("workerDone");
      }, item.waitTime);
    });
    q.emit("pushToQueue", {name: "first", waitTime: 3000});
    q.emit("pushToQueue", {name: "second", waitTime: 1000});
    q.emit("pushToQueue", {name: "third", waitTime: 6000});
  </script>

</head>
</html>
```
This would have the same output as the server code.
  
   
 